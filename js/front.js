$.extend($.easing,{
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	}
});

function viewport(){
	var e = window
		, a = 'inner';
	
	if(!('innerWidth' in window)){
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
}

function reposicionaRodape(){
	var alt = viewport().height;

	var paddingMainTop    = parseInt($('.main').css('padding-top'));
	var paddingMainBottom = parseInt($('.main').css('padding-bottom'));
	var footerHeight      = $('footer').outerHeight();
	var headerHeight      = $('header').outerHeight();

	var valor = parseInt(alt) - paddingMainTop - paddingMainBottom - footerHeight - headerHeight;
	$('.main').css('min-height', valor);
}

function posicionaNavegacao(){
	var width = viewport().width;
	var valor = (parseInt(width) - 990) / 2;
	$('#navegacao').css('right', valor);
}


function posicionaNavegacaoClipping(){
	var width = viewport().width;
	var valor = ((parseInt(width) - 990) / 2) - 130;
	$('#navegacao').css('right', valor);	
}

$('document').ready( function(){

	if($('.main-home').length){
		$('.main-home').cycle();
	}

	if(!$('.main-home').length){
		reposicionaRodape();
		$(window).resize( function(){
			reposicionaRodape();
		});
	}

	if($('.centro.detalhe').length){
		$('.centro.detalhe').waitForImages( function(){

			if($('.centro.detalhe.clippings').length)
				posicionaNavegacaoClipping();
			else
				posicionaNavegacao();

			if($('.centro.detalhe img').length > 1 && parseInt($("body").css('height')) > viewport().height){
				$('.centro.detalhe img:nth-child(2)').waypoint( function(event, direction){
					if(direction === 'down'){
						$('#navegacao').fadeIn('slow');
					}else{
						$('#navegacao').fadeOut('fast');
					}
				});
				$('#navegacao a#nav-topo').click( function(event){
					event.preventDefault();
					$('html, body').animate({ 'scrollTop' : 0}, 300);
				});
			}else{
				$('#navegacao').find('a#nav-topo').hide();
				$('#navegacao').fadeIn('slow');
			}
		});
	}

	$('.main .centro.projetos a').hover( function(){
		$(this).find('.off').fadeOut('normal', 'swing');
		$(this).find('.legenda').slideDown('normal');
	}, function(){
		$(this).find('.off').fadeIn('normal', 'swing');
		$(this).find('.legenda').slideUp('normal');
	});

});