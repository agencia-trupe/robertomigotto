$(document).ready( function(){

    $('.voltar').click( function(){ window.history.back(); });

    $('#datepicker').datepicker();
    $('#monthpicker').monthpicker();

    if($('.mostrarerro').length || $('.mostrarsucesso').length){
        console.log('erro')
        $('.mensagem').show('normal', function(){
            $(this).delay(4000).slideUp('normal');
        })
    }

    $('.delete').click( function(){
        if(!confirm('Deseja Excluir o Registro ?'))
            return false;
    });

    $('ul.lista-ordenavel').sortable({
        start: function(e, ui){
            ui.placeholder.height(ui.item.height())
                          .width(ui.item.width())
                          .css('display', 'inline-block');;
        },
        update : function () {
            serial = [];
            tabela = $('ul.lista-ordenavel').attr('data-tabela');
            $('ul.lista-ordenavel').children('li').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela }, function(resposta){
                resposta = JSON.parse(resposta);
            });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width())
            });
            return ui;
        },
        snap: true,
    }).disableSelection();

    $('.btn-move').click( function(e){e.preventDefault();});

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "comimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,separator,image",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste,advimage",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "semimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "basico",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width:660,
        height:150
    });
});