<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'contato';
		$this->imagemOriginal = FALSE;
		$this->imagemThumb = FALSE;
		
		$this->dados = array(
			'telefone',
			'endereco',
			'email',
			'mapa'
		);

		$query = $this->pegarTodos();
		$atual = $query[0]; 

		$this->dados_tratados = array(
			'mapa' => (isset($_POST['mapa']) && $_POST['mapa'] != htmlentities($atual->mapa)) ? $_POST['mapa'] : FALSE,
			'endereco' => nl2br($this->input->post('endereco'))
		);
	}
}
?>