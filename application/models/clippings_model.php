<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'clippings';
		$this->tabela_imagens = 'clippings_imagens';

		$this->imagemThumb = array(
			'dir' => '_imgs/clippings/capa/',
			'x' => '180',
			'y' => '240',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);
		$this->imagemOriginal = array(
			'dir' => '_imgs/clippings/',
			'x' => '480',
			'y' => '640',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);

		$this->dados = array(
			'titulo',
			'capa',
			'data'
		);
		$this->dados_tratados = array(
			'capa' => $this->sobeCapa(),
			'data' => formataData('01/'.$this->input->post('data'), 'br2mysql')
		);
	}

	function pegarTodos($by = 'data', $order = 'desc'){
		return $this->db->order_by($by, $order)->get($this->tabela)->result();
	}

	function sobeCapa(){
		$this->load->library('upload');

		$original = $this->imagemThumb;

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = date('YmdHis').url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->$original['corte']($original['x'], $original['y'])
		                ->save($original['dir'].$filename, TRUE);
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = $this->imagemOriginal;

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = date('YmdHis').url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->$original['corte']($original['x'], $original['y'])
		                ->save($original['dir'].$filename, TRUE);
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function imagens($id_parent, $id_imagem = FALSE){
		if(!$id_imagem){
			return $this->db->order_by('ordem', 'asc')->order_by('id', 'asc')->get_where($this->tabela_imagens, array('id_parent' => $id_parent))->result();
		}else{
			$query = $this->db->order_by('ordem', 'asc')->order_by('id', 'asc')->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}
}