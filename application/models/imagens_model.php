<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagens_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'slides';
		$this->imagemOriginal = array(
			'dir' => '_imgs/home/',
			'x' => '1920',
			'y' => '1200',
			'corte' => 'resize',
			'campo' => 'userfile'
		);
		$this->imagemThumb = FALSE;

		$this->dados = array(
			'imagem'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->get($this->tabela)->result();
	}

	/* $original = array(
		'dir' => '',
		'x' => '',
		'y' => '',
		'corte' => 'resize|resize_crop',
		'campo' => ''
	)*/
	/* $thumb = array('dir' => '', 'x' => '', 'y' => '', 'corte' => 'resize|resize_crop')*/
	function sobeImagem(){
		$this->load->library('upload');

		$original = $this->imagemOriginal;
		$thumb = $this->imagemThumb;
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '',
		  'max_width' => '1920',
		  'max_height' => '1200');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = date('YmdHis').url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->{$original['corte']}($original['x'], $original['y'])
		                ->save($original['dir'].$filename, TRUE);
		        }

		        if(isset($thumb) && $thumb !== FALSE && isset($thumb['x']) && $thumb['x'] && isset($thumb['y']) && $thumb['y']){
		        	$this->image_moo
		                ->load($original['dir'].$filename)
		                ->{$thumb['corte']}($thumb['x'], $thumb['y'])
		                ->save($thumb['dir'].$filename, TRUE);
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

	function gravarOrdem(){
		$ordem = $this->input->post('ordem');
		$ordem = explode('&', $ordem);
		foreach ($ordem as $key => $value) {
			list($resto,$id) = explode('=', $value);
			$retorno = $this->db->set('ordem', $key)->where('id', $id)->update($this->tabela);
		}
		return $retorno;
	}
}
?>
