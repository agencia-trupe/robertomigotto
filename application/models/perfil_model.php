<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'perfil';
		$this->imagemOriginal = array(
			'dir' => '_imgs/perfil/',
			'x' => '350',
			'y' => '525',
			'corte' => 'resize',
			'campo' => 'userfile'
		);
		$this->imagemThumb = FALSE;
		
		$this->dados = array(
			'texto',
			'imagem'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}
}
?>