<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos_mostras_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'projetos_mostras';
		$this->tabela_imagens = 'projetos_mostras_imagens';

		$this->imagemOriginal = array(
			'dir' => '_imgs/projetos/',
			'x' => '800',
			'y' => '99999',
			'corte' => 'resize',
			'campo' => 'userfile'
		);
		$this->imagemThumb = array(
			'dir' => '_imgs/projetos/',
			'x' => FALSE,
			'y' => FALSE,
			'corte' => 'resize',
			'campo' => 'userfile'
		);

		$this->dados = array(
			'titulo',
			'descricao',
			'capa'
		);
		$this->dados_tratados = array(
			'capa' => $this->sobeCapa()
		);
	}

	function pegarTodos($by = 'id', $order = 'desc'){
		return $this->db->order_by($by, $order)->get($this->tabela)->result();
	}

	function sobeCapa(){
		$prefixo = md5($this->tabela.$this->input->post('titulo'));
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/projetos/',
			'x' => '188',
			'y' => '188',
			'corte' => 'resize_crop',
			'campo' => 'userfile'
		);

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$original['campo']]) && $_FILES[$original['campo']]['error'] != 4){
		    if(!$this->upload->do_upload($original['campo'])){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = date('YmdHis').$prefixo.url_title($arquivo['file_name'], 'underscore', true);
		        $on_filename = 'on_'.$filename;
		        $off_filename = 'off_'.$filename;
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$on_filename);

		        if(isset($original['x']) && $original['x'] && isset($original['y']) && $original['y']){
		        	$this->image_moo
		                ->load($original['dir'].$on_filename)
		                ->{$original['corte']}($original['x'], $original['y'])
		                ->save($original['dir'].$on_filename, TRUE)
		                ->filter(IMG_FILTER_GRAYSCALE)
		                ->save($original['dir'].$off_filename, TRUE);
		        }

		        return $filename;
		    }
		}else{
		    return false;
		}
	}

}
