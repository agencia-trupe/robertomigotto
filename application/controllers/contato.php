<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$this->load->model('contato_model');
   		$data['dados'] = $this->contato_model->pegarTodos();
   		$this->load->view('contato', $data);
   }

   function enviar(){
   		$emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'smtp';
        $emailconf['smtp_host'] = 'smtp.robertomigotto.com.br';
        $emailconf['smtp_user'] = 'noreply=robertomigotto.com.br';
        //$emailconf['smtp_pass'] = 'migo137';
        $emailconf['smtp_pass'] = 'HaysnYa5272h';
        //$emailconf['smtp_port'] = '25';
        $emailconf['smtp_port'] = '587';
        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_telefone = $this->input->post('telefone');
        $u_email = $this->input->post('email');
        $u_msg = $this->input->post('mensagem');

        $from = 'noreply@robertomigotto.com.br';
        $fromname = 'Contato';

        $to = 'contato@robertomigotto.com.br';
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Contato via Site';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span><br />
                    <span class='tit'>Email :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bc)
            $this->email->cc($bc);
        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

	    $send = $this->email->send();

        if(!$send){
            echo $this->email->print_debugger();
            die();
        }

        $this->session->set_flashdata('envio', TRUE);

        $this->load->model('contato_model');
   		$data['dados'] = $this->contato_model->pegarTodos();
        $this->load->view('enviado', $data);
   }

}