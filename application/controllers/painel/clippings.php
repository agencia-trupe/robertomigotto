<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->titulo = 'Clippings';
   	$this->load->model('clippings_model', 'model');
   }

   function inserirImagensMultiplas(){

      $filenames_ar = array();
      foreach($_FILES['multi_imagens']['name'] as $k => $v)
        $filenames_ar[$k] = $k.date('YmdHis').url_title($v, 'underscore', true);

     $this->load->library('upload');
       //Configure upload.
       $this->upload->initialize(array(
         'upload_path' => $this->model->imagemOriginal['dir'],
         'allowed_types' => 'jpg|png|gif',
         'max_size' => '0',
         'max_width' => '0',
         'max_height' => '0',
        'file_name' => $filenames_ar
       ));

       //Perform upload.
       if($this->upload->do_multi_upload("multi_imagens")) {
         //Code to run upon successful upload.

         $data =$this->upload->get_multi_upload_data();

         foreach ($data as $imagem) {
           $filename = url_title($imagem['file_name'], 'underscore', true);
           rename($this->model->imagemOriginal['dir'].$imagem['file_name'] , $this->model->imagemOriginal['dir'].$filename);

           if(isset($this->model->imagemOriginal['x']) && $this->model->imagemOriginal['x'] && isset($this->model->imagemOriginal['y']) && $this->model->imagemOriginal['y']){
             $this->image_moo
                   ->load($this->model->imagemOriginal['dir'].$filename)
                   ->resize_crop($this->model->imagemOriginal['x'], $this->model->imagemOriginal['y'])
                   ->save($this->model->imagemOriginal['dir'].$filename, TRUE);
           }

           if(isset($this->model->imagemThumb) && $this->model->imagemThumb !== FALSE && isset($this->model->imagemThumb['x']) && $this->model->imagemThumb['x'] && isset($this->model->imagemThumb['y']) && $this->model->imagemThumb['y']){
             $this->image_moo
                   ->load($this->model->imagemOriginal['dir'].$filename)
                   ->resize($this->model->imagemThumb['x'], $this->model->imagemThumb['y'])
                   ->save($this->model->imagemThumb['dir'].$filename, TRUE);
           }

           $this->db->set('imagem', $filename)
                     ->set('id_parent', $this->input->post('id_parent'))
                     ->insert($this->model->tabela_imagens);
         }

         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
         redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
       }else{
         die('Erro ao enviar as imagens.');
       }
    }
}