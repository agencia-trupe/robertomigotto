<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();
   }

   function index(){
      $data['titulo'] = 'Projetos';
    	$this->load->view('painel/'.$this->router->class.'/lista', $data);
   }

}