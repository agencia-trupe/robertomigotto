<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imagens extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->titulo = 'Imagens da Home';
   	$this->load->model('imagens_model', 'model');
   }

   function ordem(){
   	$data['registros'] = $this->model->pegarTodos();
   	$data['titulo'] = "Ordenar Imagens da Home";
   	$this->load->view('painel/imagens/ordem', $data);
   }

   function gravarOrdem(){
    if($this->model->gravarOrdem()){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Ordem alterada com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar ordem das imagens');
    }

    redirect('painel/'.$this->router->class.'/ordem', 'refresh');
   }

}