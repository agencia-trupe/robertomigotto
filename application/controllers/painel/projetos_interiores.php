<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos_interiores extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = 'Projetos de Arquitetura';
    	$this->unidade = 'Projeto';
		$this->campo_1 = 'Título';
		$this->campo_2 = 'Descrição';
		$this->load->model('projetos_interiores_model', 'model');		
	}

}