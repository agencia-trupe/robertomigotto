<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$this->load->model('perfil_model');
   		$data['texto'] = $this->perfil_model->pegarTodos();
   		$this->load->view('perfil', $data);
   }

}