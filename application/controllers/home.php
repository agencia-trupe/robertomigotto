<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
   		$this->load->model('imagens_model');
   		$data['imagens'] = $this->imagens_model->pegarTodos();
   		$this->load->view('home', $data);
   }

}