<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projetos extends MY_Frontcontroller {

   function __construct(){
         parent::__construct('', 'waypoints.min');
   }

   function index(){
   		redirect($this->router->class.'/arquitetura');
   }

   function arquitetura($id = FALSE){
      $this->load->model('projetos_arquitetura_model', 'arquitetura');

      if($id){
         $data['projeto'] = $this->arquitetura->pegarPorId($id);
         $data['proximo'] = $this->arquitetura->proximoProjeto($id);
         $data['imagens'] = $this->arquitetura->imagens($id);
         if(!$data['projeto'])
            redirect('projetos/arquitetura');
         $this->load->view('projetos-detalhe', $data);
      }else{
         $data['projetos'] = $this->arquitetura->pegarTodos();
         $this->load->view('projetos', $data);
      }
   }

   function interiores($id = FALSE){
      $this->load->model('projetos_interiores_model', 'interiores');

      if($id){
         $data['projeto'] = $this->interiores->pegarPorId($id);
         $data['proximo'] = $this->interiores->proximoProjeto($id);
         $data['imagens'] = $this->interiores->imagens($id);
         if(!$data['projeto'])
            redirect('projetos/interiores');
         $this->load->view('projetos-detalhe', $data);
      }else{
         $data['projetos'] = $this->interiores->pegarTodos();
         $this->load->view('projetos', $data);
      }
   }

   function comercial($id = FALSE){
      $this->load->model('projetos_comercial_model', 'comercial');

      if($id){
         $data['projeto'] = $this->comercial->pegarPorId($id);
         $data['proximo'] = $this->comercial->proximoProjeto($id);
         $data['imagens'] = $this->comercial->imagens($id);
         if(!$data['projeto'])
            redirect('projetos/comercial');
         $this->load->view('projetos-detalhe', $data);
      }else{
         $data['projetos'] = $this->comercial->pegarTodos();
         $this->load->view('projetos', $data);
      }
   }

   function mostras($id = FALSE){
      $this->load->model('projetos_mostras_model', 'mostras');

      if($id){
         $data['projeto'] = $this->mostras->pegarPorId($id);
         $data['proximo'] = $this->mostras->proximoProjeto($id);
         $data['imagens'] = $this->mostras->imagens($id);
         if(!$data['projeto'])
            redirect('projetos/mostras');
         $this->load->view('projetos-detalhe', $data);
      }else{
         $data['projetos'] = $this->mostras->pegarTodos();
         $this->load->view('projetos', $data);
      }
   }

}