<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clipping extends MY_Frontcontroller {

      function __construct(){
         parent::__construct('', 'waypoints.min');
         $this->load->model('clippings_model');
      }

   	function index(){
   		$data['clippings'] = $this->clippings_model->pegarTodos();

   		$this->load->view('clipping', $data);
   	}

   	function detalhes($id = false){
   		if(!$id)
   			redirect('clipping');

   		$data['parent'] = $this->clippings_model->pegarPorId($id);
   		$data['detalhe'] = $this->clippings_model->imagens($id);
         $data['proximo'] = $this->clippings_model->proximoProjeto($id);

   		if(!$data['detalhe'])
   			redirect('clipping');
   		
   		$this->load->view('clipping-detalhe', $data);
   	}

}