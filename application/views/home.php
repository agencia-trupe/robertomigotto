<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?=CLIENTE?></title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'fontface/stylesheet', $this->router->class, 'responsive'))?>


  <?if(ENVIRONMENT == 'development'):?>

    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.6.4.min', $this->router->class))?>

  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.6.4.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>

  <header class="header-home">

    <nav>

      <img src="_imgs/layout/robertomigotto-home.png" alt="Roberto Migotto - Arquitetura e Interiores">

      <ul>
        <li><a href="projetos" title="Projetos" id="mn-projetos" <?if($this->router->class=='projetos')echo" class='ativo'"?>>projetos</a></li>
        <li><a href="perfil" title="Perfil" id="mn-perfil" <?if($this->router->class=='perfil')echo" class='ativo'"?>>perfil</a></li>
        <li><a href="clipping" title="Clippings" id="mn-clipping" <?if($this->router->class=='clipping')echo" class='ativo'"?>>clipping</a></li>
        <li><a href="contato" title="Entre em Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
      </ul>
    </nav>

  </header>

  <div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

    <?php foreach ($imagens as $key => $value): ?>
      <img src="_imgs/home/<?=$value->imagem?>">
    <?php endforeach ?>

  </div> <!-- fim da div main -->

  <footer>

    <span>&copy; <?=date('Y')?> Roberto Migotto &bull; Todos os Direitos Reservados &bull;</span> <a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">Criação de Sites : Trupe Agência Criativa</a>

  </footer>


  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('cycle','front'))?>

</body>
</html>