<header class="header-interna">

	<div class="centro">

		<a id="link-home" href="<?=base_url()?>" title="Página Inicial"><img src="_imgs/layout/robertomigotto-internas.png" alt="<?=CLIENTE?>"></a>

		<nav>
			<ul>
				<?php if ($this->router->class=='projetos'): ?>
					<ul class="submenu">
						<li><a href="projetos/arquitetura" title="Projetos Arquitetônicos" id="mn-arquitetura" <?if($this->router->method=='arquitetura')echo" class='ativo'"?>>_arquitetura</a></li>
						<li><a href="projetos/interiores" title="Projetos de Interiores" id="mn-interiores" <?if($this->router->method=='interiores')echo" class='ativo'"?>>_interiores</a></li>
						<li><a href="projetos/comercial" title="Projetos Comerciais" id="mn-comercial" <?if($this->router->method=='comercial')echo" class='ativo'"?>>_comercial</a></li>
						<li><a href="projetos/mostras" title="Mostras" id="mn-mostras" <?if($this->router->method=='mostras')echo" class='ativo'"?>>_mostras</a></li>
					</ul>
				<?php else: ?>
					<li><a href="projetos" title="Projetos" id="mn-projetos" <?if($this->router->class=='projetos')echo" class='ativo'"?>>projetos</a></li>	
				<?php endif ?>
				<li><a href="perfil" title="Perfil Roberto Migotto" id="mn-perfil" <?if($this->router->class=='perfil')echo" class='ativo'"?>>perfil</a></li>
				<li><a href="clipping" title="Clippings" id="mn-clipping" <?if($this->router->class=='clipping')echo" class='ativo'"?>>clipping</a></li>
				<li><a href="contato" title="Entre em Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
				
			</ul>
		</nav>

	</div>
	
</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">