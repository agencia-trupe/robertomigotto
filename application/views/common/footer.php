
  </div> <!-- fim da div main -->

  <footer>
    <?php if ($this->router->class!='home'): ?>
    <nav>
      <a href="projetos">projetos</a>
      <a href="perfil">perfil</a>
      <a href="clipping">clipping</a>
      <a href="contato">contato</a>
    </nav>
    <?php endif; ?>
    <span>&copy; <?=date('Y')?> Roberto Migotto &bull; Todos os Direitos Reservados &bull;</span> <a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">Criação de Sites : Trupe Agência Criativa</a>
  </footer>


  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('','waitforimages', 'front'))?>

</body>
</html>
