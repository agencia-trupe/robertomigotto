<div class="centro detalhe">

	<div class="texto">
		<h1><?=$projeto->titulo?></h1>
		<p><?=nl2br($projeto->descricao)?></p>
	</div>

	<?php foreach ($imagens as $key => $value): ?>
		<img src="_imgs/projetos/<?=$value->imagem?>" alt="<?=$projeto->titulo?>">
	<?php endforeach ?>
	
	<div id="navegacao">
		<?php if ($proximo): ?>
			<a href="<?=$this->router->class?>/<?=$this->router->method?>/<?=$proximo?>" id="nav-next" title="Próximo Projeto">Próximo Projeto</a>
		<?php endif ?>
		<a href="<?=$this->router->class?>/<?=$this->router->method?>" id="nav-menu" title="Voltar ao Menu">Voltar ao Menu</a>
		<a href="" id="nav-topo" title="Voltar ao Topo">Voltar ao Topo</a>
	</div>

</div>