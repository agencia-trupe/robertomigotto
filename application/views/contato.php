<div class="centro contato">
	
	<div class="formulario coluna">
		<h1>FALE CONOSCO</h1>

		<form method="post" action="contato/enviar" id="form-contato">
			
			<input type="text" name="nome" placeholder="NOME" class="placeholder" required id="nome">

			<input type="email" name="email" placeholder="E-MAIL" class="placeholder" required id="email">

			<input type="text" name="telefone" placeholder="TELEFONE" class="placeholder">

			<textarea name="mensagem" placeholder="MENSAGEM" class="placeholder" required></textarea>

			<input type="submit" value="ENVIAR &raquo;">

		</form>

	</div>

	<div class="mapa coluna">
		
		<h2><?=$dados[0]->telefone?></h2>

		<p><?=$dados[0]->endereco?></p>

		<?=viewGMaps($dados[0]->mapa, 500, 350)?>

	</div>

</div>
<script defer>

	$('document').ready( function(){

	    if (!Modernizr.input.placeholder){
	        
	        $('.placeholder').each(function(){
	            if ($(this).val() == ''){
	                $(this).val( $(this).attr('placeholder') );
	            }
	        });
	 
	        $('.placeholder').focus(function(){
	            if ($(this).val() == $(this).attr('placeholder')){
	                $(this).val('');
	                $(this).removeClass('placeholder');
	            }
	        }).blur(function(){
	            if ($(this).val() == '' || $(this).val() == $(this).attr('placeholder')){
	                $(this).val($(this).attr('placeholder'));
	                $(this).addClass('placeholder');
	            }
	        });
	    }			

		$('#form-contato').submit( function(){
			var er = /([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]*(.){1}[a-zA-Z]{2,4})+/;
			if($('#nome').val() == '' || $('#nome') == $('#nome').attr('placeholder')){
				alert('Informe seu Nome');
				return false;
			}
			if($('#email').val() == '' || $('#email') == ('#email').attr('placeholder')){
				alert('Informe seu E-mail');
				return false;
			}
			if(!er.test($('#email').val())){
				alert('Informe um E-mail Válido');
				return false;	
			}
			$(this).find('.placeholder').each(function(){
		        if ($(this).val() == $(this).attr('placeholder'))
		            $(this).val('');
		    });	
			return true;
		});

	});
	
</script>