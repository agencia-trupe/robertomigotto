<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Imagens</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/ordem')?>" class="lista">Ordenar Imagens</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Imagem</a>
</div>


<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

	<div id="dialog"></div>

	<div id="alerta" class="mensagem">
		Tamanho máximo : 700kb - Dimensões Máximas : 1920 x 1200px
	</div>

	<label>
		Imagem<br>
		<input type="file" name="userfile" required>
	</label>

	<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
</form>