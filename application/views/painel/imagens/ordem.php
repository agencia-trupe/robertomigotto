<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Imagens</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/ordem')?>" class="lista active">Ordenar Imagens</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Imagem</a>
</div>

<div id="alerta" class="mensagem">
	Para ordenar as imagens, clique e arraste.
</div>

<ol>
	<? foreach ($registros as $key => $value): ?>
		<li id="lista_<?=$value->id?>"><img style="width:200px;" src="_imgs/home/<?=$value->imagem?>"></li>
	<? endforeach; ?>
</ol>

<form method="post" action="painel/imagens/gravarOrdem">

	<input type="hidden" name="ordem" id="ordem">

	<input type="submit" value="GRAVAR ORDEM"> <input type="button" class="voltar" value="VOLTAR">

</form>

<script defer>
	$('document').ready( function(){

		$('ol').sortable({
			update : function(event, ui){
				console.log($(this).sortable('serialize'));
				$('#ordem').val($(this).sortable('serialize'));
			}
		});

	});
</script>

<style>
	ol li img:hover{ cursor:move; }
</style>