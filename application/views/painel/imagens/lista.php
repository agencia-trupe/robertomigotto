<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Imagens</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/ordem')?>" class="lista">Ordenar Imagens</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Imagem</a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Imagem</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><img src="_imgs/home/<?=$value->imagem?>" style="width:150px;"></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

<?endif;?>