<h1><?=$titulo?></h1>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Telefone<br>
			<input type="text" name="telefone" required value="<?=$registro->telefone?>">
		</label>

		<label>
			Endereço<br>
			<textarea name="endereco" required><?=br2nl($registro->endereco)?></textarea>
		</label>

		<label>
			Email<br>
			<input type="text" name="email" required value="<?=$registro->email?>">
		</label>

		<label>
			Código de Incorporação do Google Maps<br>
			<input type="text" name="mapa" required value="<?=htmlentities($registro->mapa)?>">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Telefone<br>
			<input type="text" name="telefone" required>
		</label>

		<label>
			Endereço<br>
			<textarea name="endereco" required></textarea>
		</label>

		<label>
			Email<br>
			<input type="text" name="email" required>
		</label>

		<label>
			Código de Incorporação do Google Maps<br>
			<input type="text" name="mapa" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>