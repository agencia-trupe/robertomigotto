<h1><?=$titulo?></h1>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Telefone</th>
				<th>Endereço</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->telefone?></td>
				<td><?=$value->endereco?></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

<?endif;?>