<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/projetos_arquitetura/index')?>" class="lista active">Arquitetura</a>
	<a href="<?=base_url('painel/projetos_comercial/index')?>" class="lista">Comercial</a>
	<a href="<?=base_url('painel/projetos_interiores/index')?>" class="lista">Interiores</a>
	<a href="<?=base_url('painel/projetos_mostras/index')?>" class="lista">Mostras</a>
</div>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Projetos de Arquitetura</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Projeto de Arquitetura</a>
</div>


<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Nome do Projeto<br>
			<input type="text" name="titulo" value="<?=$registro->titulo?>" required>
		</label>

		<label>
			Descrição<br>
			<textarea name="descricao"><?=$registro->descricao?></textarea>
		</label>

		<label>
			Capa do Projeto<br>
			<img src="_imgs/projetos/on_<?=$registro->capa?>"><br>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Nome do Projeto<br>
			<input type="text" name="titulo" required>
		</label>

		<label>
			Descrição<br>
			<textarea name="descricao"></textarea>
		</label>

		<label>
			Capa do Projeto<br>
			<input type="file" name="userfile" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>