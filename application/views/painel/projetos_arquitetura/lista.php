<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/projetos_arquitetura/index')?>" class="lista active">Arquitetura</a>
	<a href="<?=base_url('painel/projetos_comercial/index')?>" class="lista">Comercial</a>
	<a href="<?=base_url('painel/projetos_interiores/index')?>" class="lista">Interiores</a>
	<a href="<?=base_url('painel/projetos_mostras/index')?>" class="lista">Mostras</a>
</div>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Projetos de Arquitetura</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Projeto de Arquitetura</a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th><?=$campo_1?></th>
				<th><?=$campo_2?></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr>
				<td><?=$value->titulo?></td>
				<td><?=olho($value->descricao, 10)?></td>
				<td><a class="imagens" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$value->id)?>">Imagens</a></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

<?endif;?>