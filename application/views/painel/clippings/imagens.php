<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Clippings</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Clipping</a>
</div>

<form method="post" action="<?='painel/'.$this->router->class.'/inserirImagensMultiplas'?>" enctype="multipart/form-data">

	<label>
		Imagem<br>
		<input type="file" name="multi_imagens[]" multiple>
	</label>

	<input type="hidden" name="id_parent" value="<?=$parent->id?>">

	<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
</form>

<br>

<?if($imagens):?>

	<ul class="lista-ordenavel" data-tabela="clippings_imagens" style="list-style:none; display:block;">
		<? foreach ($imagens as $key => $value): ?>
			<li style="width:200px; display:inline-block; vertical-align: top; margin: 0 0 8px 0;" id="row_<?php echo $value->id ?>">
				<img src="_imgs/clippings/<?=$value->imagem?>" style="width:150px; display:block; margin: 0 auto 5px auto;"></td>
				<a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluirImagem/'.$value->id.'/'.$parent->id)?>">Excluir</a>
			</li>
		<? endforeach; ?>
	</ul>

	<br><br>

	<div style='text-align:center; padding: 20px 0;'>
		<a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluirTodasAsImagens/'.$parent->id)?>">Excluir todas as imagens desse projeto</a>
	</div>

<?endif;?>