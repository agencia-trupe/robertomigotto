<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Clippings</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Clipping</a>
</div>


<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Nome da Publicação<br>
			<input type="text" name="titulo" value="<?=$registro->titulo?>" required>
		</label>

		<label>
			Data da Publicação<br>
			<input type="text" maxlength="7" id="monthpicker" name="data" value="<?= preg_replace('/01\//', '', formataData($registro->data.'-01', 'mysql2br'), 1)?>">
		</label>

		<label>
			Capa da Publicação<br>
			<img src="_imgs/clippings/capa/<?=$registro->capa?>"><br>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Nome da Publicação<br>
			<input type="text" name="titulo" required>
		</label>

		<label>
			Data da Publicação<br>
			<input type="text" maxlength="7" id="monthpicker" name="data">
		</label>

		<label>
			Capa da Publicação<br>
			<input type="file" name="userfile" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>