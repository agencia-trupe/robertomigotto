<h1><?=$titulo?></h1>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Texto<br>
			<textarea class="basico" name="texto"><?=$registro->texto?></textarea>
		</label>

		<label>
			Imagem<br>
			<img src="_imgs/perfil/<?=$registro->imagem?>"><br>
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Texto<br>
			<textarea required name="texto"></textarea>
		</label>

		<label>
			Imagem<br>
			<input type="file" name="userfile" required>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>