
<!-- Shell -->
<div id="shell">
	
	<!-- Header -->
	<div id="header">
		<h1><a href="<?=base_url('painel/home')?>">Painel - <?=CLIENTE?></a></h1>
		<div class="right">
			<p>Bem vindo, <strong><?=$this->session->userdata('username')?></strong></p>
			<p class="small-nav"><a href="<?=base_url('painel/usuarios')?>" <?if($this->router->class=='usuarios')echo " class='active'"?>>Usuários</a> / <a href="<?=base_url('painel/home/logout')?>">Log Out</a></p>
		</div>
	</div>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<ul>
		    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
		    <li><a href="<?=base_url('painel/imagens')?>" <?if($this->router->class=='imagens')echo " class='active'"?>><span>Imagens da Home</span></a></li>
		    <li><a href="<?=base_url('painel/projetos')?>" <?if($this->router->class=='projetos'||$this->router->class=='projetos_arquitetura'||$this->router->class=='projetos_comercial'||$this->router->class=='projetos_interiores'||$this->router->class=='projetos_mostras')echo " class='active'"?>><span>Projetos</span></a></li>
		    <li><a href="<?=base_url('painel/perfil')?>" <?if($this->router->class=='perfil')echo " class='active'"?>><span>Perfil</span></a></li>
		    <li><a href="<?=base_url('painel/clippings')?>" <?if($this->router->class=='clippings')echo " class='active'"?>><span>Clipping</span></a></li>
		    <li><a href="<?=base_url('painel/contato')?>" <?if($this->router->class=='contato')echo " class='active'"?>><span>Contato</span></a></li>
		</ul>
	</div>
	<!-- End Navigation -->
	
	<!-- Content -->
	<div id="content">

	<?if(isset($mostrarerro_mensagem) && $mostrarerro_mensagem):?>
		<div class="mensagem" id="erro"><?=$mostrarerro_mensagem?></div>
	<?elseif(isset($mostrarsucesso_mensagem) && $mostrarsucesso_mensagem):?>
		<div class="mensagem" id="sucesso"><?=$mostrarsucesso_mensagem?></div>
	<?endif;?>