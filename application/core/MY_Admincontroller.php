<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Admincontroller extends CI_controller {

    var $headervar;
    var $menuvar;
    var $footervar;
    var $titulo,$unidade,$campo_1,$campo_2,$campo_3;

    function __construct() {
        parent::__construct();

        if(!$this->session->userdata('logged_in'))
            redirect('painel/login');

        if($this->session->flashdata('mostrarerro') === true){
            $this->headervar['mostrarerro'] = true;
            $this->menuvar['mostrarerro_mensagem'] = $this->session->flashdata('mostrarerro_mensagem');
        }else{
            $this->headervar['mostrarerro'] = false;
            $this->menuvar['mostrarerro_mensagem'] = '';
        }

        if($this->session->flashdata('mostrarsucesso') === true){
            $this->headervar['mostrarsucesso'] = true;
            $this->menuvar['mostrarsucesso_mensagem'] = $this->session->flashdata('mostrarsucesso_mensagem');
        }else{
            $this->headervar['mostrarsucesso'] = false;
            $this->menuvar['mostrarsucesso_mensagem'] = '';
        }
    }

   function index(){
    $data['registros'] = $this->model->pegarTodos();

      $data['titulo'] = $this->titulo;
      $data['unidade'] = $this->unidade;
      $data['campo_1'] = $this->campo_1;
      $data['campo_2'] = $this->campo_2;
      $data['campo_3'] = $this->campo_3;
    $this->load->view('painel/'.$this->router->class.'/lista', $data);
   }

   function form($id = false){
    if($id){
        $data['registro'] = $this->model->pegarPorId($id);
        if(!$data['registro'])
            redirect('painel/'.$this->router->class);
    }else{
        $data['registro'] = FALSE;
    }

    $data['titulo'] = $this->titulo;
    $data['unidade'] = $this->unidade;
    $this->load->view('painel/'.$this->router->class.'/form', $data);
   }

   function inserir(){
    if($this->model->inserir()){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
    }

    redirect('painel/'.$this->router->class.'/index', 'refresh');
   }

   function alterar($id){
    if($this->model->alterar($id)){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
    }
    redirect('painel/'.$this->router->class.'/index', 'refresh');
   }

   function excluir($id){
    if($this->model->excluir($id)){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
    }

    redirect('painel/'.$this->router->class.'/index', 'refresh');
   }

   function imagens($id_parent, $id_imagem =  FALSE){
    if($id_imagem){
        $data['parent'] = $this->model->pegarPorId($id_parent);
        $data['imagens'] = $this->model->imagens($id_parent);
        $data['registro'] = $this->model->imagens($id_parent, $id_imagem);
        if(!$data['parent'])
            redirect('painel/'.$this->router->class);
    }else{
        $data['parent'] = $this->model->pegarPorId($id_parent);
        $data['imagens'] = $this->model->imagens($id_parent);
        $data['registro'] = FALSE;
    }

    if(isset($data['parent']->titulo))
        $titulo_atual = $data['parent']->titulo;
    elseif(isset($data['parent']->nome))
        $titulo_atual = $data['parent']->nome;
    else
        $titulo_atual = $this->titulo;

    $data['campo_1'] = "Imagem";
    $data['titulo'] = 'Galeria de imagens de : '.$titulo_atual;
    $data['unidade'] = $this->unidade;
    $this->load->view('painel/'.$this->router->class.'/imagens', $data);
   }

  function inserirImagensMultiplas(){

    // Filenames array
      $filenames_ar = array();
      foreach($_FILES['multi_imagens']['name'] as $k => $v)
        $filenames_ar[$k] = $k.date('YmdHis').url_title($v['name'], 'underscore', true);

      //Configure upload.
      $this->upload->initialize(array(
        'upload_path' => $this->model->imagemOriginal['dir'],
        'allowed_types' => 'jpg|png|gif',
  		  'max_size' => '0',
  		  'max_width' => '0',
  		  'max_height' => '0',
        'file_name' => $filenames_ar
      ));

      //Perform upload.
      if($this->upload->do_multi_upload("multi_imagens")) {
        //Code to run upon successful upload.

        $data =$this->upload->get_multi_upload_data();

        foreach ($data as $imagem) {
          $filename = date('YmdHis').url_title($imagem['file_name'], 'underscore', true);
          rename($this->model->imagemOriginal['dir'].$imagem['file_name'] , $this->model->imagemOriginal['dir'].$filename);

          if(isset($this->model->imagemOriginal['x']) && $this->model->imagemOriginal['x'] && isset($this->model->imagemOriginal['y']) && $this->model->imagemOriginal['y']){
            $this->image_moo
                  ->load($this->model->imagemOriginal['dir'].$filename)
                  ->resize($this->model->imagemOriginal['x'], $this->model->imagemOriginal['y'])
                  ->save($this->model->imagemOriginal['dir'].$filename, TRUE);
          }

          $this->db->set('imagem', $filename)
                    ->set('id_parent', $this->input->post('id_parent'))
                    ->insert($this->model->tabela_imagens);
        }

        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
      }else{
        die('Erro ao enviar as imagens.');
      }
   }

   function inserirImagem(){
    if($this->model->inserirImagem()){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir imagem');
    }

    redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
   }

   function editarImagem($id_imagem){
    if($this->model->editarImagem($id_imagem)){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar imagem');
    }

    redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
   }

   function excluirImagem($id_imagem, $id_album){
    if($this->model->excluirImagem($id_imagem)){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagem');
    }

    redirect('painel/'.$this->router->class.'/imagens/'.$id_album, 'refresh');
   }

  function excluirTodasAsImagens($id_parent){
    if($this->model->excluirTodasAsImagens($id_parent)){
        $this->session->set_flashdata('mostrarsucesso', true);
        $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagens do Projeto excluídas com sucesso');
    }else{
        $this->session->set_flashdata('mostrarerro', true);
        $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagens do projeto');
    }

    redirect('painel/'.$this->router->class.'/imagens/'.$id_parent, 'refresh');
   }




  function _output($output){
    echo $this->load->view('painel/common/header', $this->headervar, TRUE).
         $this->load->view('painel/common/menu', $this->menuvar, TRUE).
         $output.
         $this->load->view('painel/common/footer', $this->footervar, TRUE);
  }
}
?>
